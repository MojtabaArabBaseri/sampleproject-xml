plugins {
    id 'com.android.application'
    id 'org.jetbrains.kotlin.android'
    id 'androidx.navigation.safeargs'
    id 'kotlin-android'
    id 'kotlin-parcelize'
    id 'kotlin-kapt'
    id 'com.google.dagger.hilt.android'
    id 'dagger.hilt.android.plugin'
}
android {
    namespace 'ir.millennium.sampleProject'
    compileSdk 34
//    buildToolsVersion '30.0.3'

    defaultConfig {
        applicationId "ir.millennium.sampleProject"

        minSdk 21
        targetSdk 34
        versionCode 1
        versionName "1.0.0"

        testInstrumentationRunner "ir.millennium.sampleProject.HiltTestRunner"
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled true

        testOptions { // if We want to use JUnit5 then this code should be active
            unitTests.all {
                useJUnitPlatform()
            }
        }
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
        debug {
            minifyEnabled false
//            shrinkResources true
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }

    buildFeatures {
        dataBinding true
        viewBinding = true
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    compileOptions {
        // Flag to enable support for the new language APIs
        coreLibraryDesugaringEnabled = true
        // Sets Java compatibility to Java 8
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    lintOptions {
        checkReleaseBuilds false
        abortOnError false
    }

    hilt {
        enableTransformForLocalTests = true
    }

    packagingOptions {
        resources.excludes.add("META-INF/*")
    }
}

dependencies {
//    debugImplementation 'com.squareup.leakcanary:leakcanary-android:2.7'
    implementation 'com.android.support.constraint:constraint-layout:2.0.4'
    implementation 'com.google.android.material:material:1.4.0'
    implementation 'io.github.inflationx:calligraphy3:3.1.1'
    implementation 'io.github.inflationx:viewpump:2.0.3'
    implementation 'com.nineoldandroids:library:2.4.0'
    implementation 'androidx.multidex:multidex:2.0.1'
    implementation 'org.greenrobot:essentials:3.0.0-RC1'
    implementation 'de.greenrobot:java-common:2.3.1'
    implementation 'com.nostra13.universalimageloader:universal-image-loader:1.9.5'
    implementation("com.mikepenz:materialdrawer:6.0.8@aar") { transitive = true }
    implementation 'com.daimajia.slider:library:1.1.5@aar'
    implementation 'com.r0adkll:slidableactivity:2.0.6'
    implementation 'com.daimajia.androidanimations:library:2.4'
    implementation 'com.jsibbold:zoomage:1.3.1'
    implementation 'com.github.santaevpavel:OutlineSpan:0.1.1'
    implementation 'com.github.jorgecastilloprz:fabprogresscircle:1.01@aar'
    implementation group: 'commons-codec', name: 'commons-codec', version: '1.15'
    implementation 'com.jakewharton.timber:timber:5.0.1'
    implementation 'jp.wasabeef:recyclerview-animators:4.0.2'

    def retrofitVersion = "2.9.0"
    implementation "com.squareup.retrofit2:retrofit:$retrofitVersion"
    implementation "com.squareup.retrofit2:converter-gson:$retrofitVersion"

    def okHttpVersion = "4.9.3"
    implementation(platform("com.squareup.okhttp3:okhttp-bom:$okHttpVersion"))
    implementation "com.squareup.okhttp3:okhttp:$okHttpVersion"
    implementation "com.squareup.okhttp3:logging-interceptor:$okHttpVersion"

    def lifecycleVersion = "1.1.1"
    implementation "android.arch.lifecycle:extensions:$lifecycleVersion"
    //noinspection LifecycleAnnotationProcessorWithJava8
    annotationProcessor "android.arch.lifecycle:compiler:$lifecycleVersion"
    implementation 'com.github.mukeshsolanki:android-otpview-pinview:2.0.1'
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'androidx.constraintlayout:constraintlayout:2.1.4'
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    implementation 'androidx.appcompat:appcompat:1.5.1'

    //nav components
    def nav_version = "2.4.2"
    implementation "androidx.navigation:navigation-fragment-ktx:$nav_version"
    implementation "androidx.navigation:navigation-ui-ktx:$nav_version"
    implementation "androidx.navigation:navigation-dynamic-features-fragment:$nav_version"
    androidTestImplementation "androidx.navigation:navigation-testing:$nav_version"

    //material lib
    implementation 'androidx.cardview:cardview:1.0.0'
    implementation 'com.stephentuso:welcome:1.4.1'
    implementation 'com.github.samanzamani.persiandate:PersianDate:0.8'
    implementation 'com.github.JakeWharton:ViewPagerIndicator:2.4.1'

    implementation("androidx.fragment:fragment-ktx:1.5.5")

    def lifecycle_version = "2.5.1"
    def coroutines_version = "1.6.4"
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.8.22"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutines_version"
    coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:1.1.5")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:$lifecycle_version")
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-common-java8:$lifecycle_version"
    implementation "androidx.window:window:1.0.0-beta02"

    implementation 'com.android.databinding:viewbinding:8.1.0'

    def room_version = "2.6.0"
    implementation "androidx.room:room-runtime:$room_version"
    implementation "androidx.room:room-ktx:$room_version"
    kapt "androidx.room:room-compiler:$room_version"

    //Hilt
    def hilt_version = "2.48.1"
    implementation "com.google.dagger:hilt-android:$hilt_version"
    kapt "com.google.dagger:hilt-compiler:$hilt_version"
    androidTestImplementation "com.google.dagger:hilt-android-testing:$hilt_version"
    kaptAndroidTest "com.google.dagger:hilt-android-compiler:$hilt_version"

    def glide_version = "4.12.0"
    implementation "com.github.bumptech.glide:glide:$glide_version"
    implementation "com.github.bumptech.glide:okhttp3-integration:$glide_version"
    kapt "com.github.bumptech.glide:compiler:$glide_version"

//    // Local Unit Tests
    debugImplementation "junit:junit:4.13.2"
    debugImplementation "androidx.arch.core:core-testing:2.2.0"
    debugImplementation "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.7.2"
    debugImplementation "com.google.truth:truth:1.1.2"
    debugImplementation "org.mockito:mockito-core:3.10.0"
    debugImplementation 'org.mockito.kotlin:mockito-kotlin:3.2.0'
    debugImplementation "org.junit.jupiter:junit-jupiter-api:5.9.3"
    debugImplementation 'org.junit.jupiter:junit-jupiter:5.9.3'
    testRuntimeOnly "org.junit.jupiter:junit-jupiter-engine:5.9.3"
    debugImplementation "org.junit.jupiter:junit-jupiter-params:5.9.3"
    debugImplementation "androidx.fragment:fragment-testing:1.6.0"
    testImplementation 'org.mockito:mockito-inline:2.13.0'

//    // Instrumented Unit Tests
    androidTestImplementation "junit:junit:4.13.2"
    androidTestImplementation "com.linkedin.dexmaker:dexmaker-mockito:2.12.1"
    androidTestImplementation "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.7.2"
    androidTestImplementation "androidx.arch.core:core-testing:2.2.0"
    androidTestImplementation "com.google.truth:truth:1.1.2"
    androidTestImplementation 'androidx.test.ext:junit:1.1.5'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.5.1'
    androidTestImplementation "org.mockito:mockito-core:3.10.0"
    androidTestImplementation 'app.cash.turbine:turbine:1.0.0'
}